import { Component,OnInit } from '@angular/core';
import { PerfilService } from './services/perfil.service';
import { Router } from '@angular/router';
import { User } from './models/user';
import { from } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [PerfilService]
})
export class AppComponent implements OnInit{
  
  title = 'blogManagerFront';

  val:String;

  constructor(public perfilService:PerfilService,public router:Router){}

  ngOnInit(){
    this.setVisibleHeader();
  }

  setVisibleHeader():void{
    console.log(this.perfilService.userPerfil._id)
    /*/if(this.perfilService.userPerfil._id == ''){
      this.visibleHeader=false;
      console.log("res")
    }
    else{
      this.visibleHeader=true
      console.log("ser")
    }*/

  }

  recibirMensaje(event:String):void{
    this.val = event;
  }

  singOut(){
    this.perfilService.userPerfil = new User();
    localStorage.setItem('idPerfil',null);
    this.router.navigate(['angularBlog/user']);
  }
}

