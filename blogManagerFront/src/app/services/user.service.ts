import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { HttpClient } from '@angular/common/http';
import { ImageUser } from '../models/ImageUser';


@Injectable({
    providedIn: 'root'
})

export class UserService {

    constructor(private http:HttpClient){
        this.oneUser = new User();
    }

    readonly api = 'http://localhost:3000/api/users'

    oneUser:User;

    usersArray:User[];

    getAllUsers(){
        return this.http.get(this.api);
    }

    getOneUser(idUser:String){
        return this.http.get(this.api+`/${idUser}`);
    }

    postUser(user:User,file:File){
        const form = new FormData();
        let imageUser = new ImageUser(); 
        let name = user.name as string;
        user.fileName = file.name;
        form.append('name',user.name as string);
        form.append('nameUser',user.nameUser as string);
        form.append('typeUser',user.typeUser as string);
        form.append('password',user.password as string);
        form.append('email',user.email as string);
        form.append('fileName',user.fileName as string);
        form.append('file',file,'form-data');

        //console.log(file);
        //user.file = file;
        //user.imageUser.fileName = 

        return this.http.post(this.api+`/create`,form);
    }

    getUserEmailAndPassword(user:User){
        return this.http.get(this.api+`/${user.email}/${user.password}`);
    }

    getUserEmail(email:String){
        return this.http.get(this.api+`/email/${email}`);
        
    }

}