import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Blog } from '../models/blog';
 

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  readonly api = 'http://localhost:3000/api/blogs'; 

  theBlog:Blog;
  blogs:Blog[];

  constructor(private http : HttpClient) {
    this.theBlog = new Blog();
  }

  getBlogs = () =>{
    return this.http.get(this.api);
  }

  getOneBlog = (id:String) =>{
    return this.http.get(this.api+`/${id}`)
  }

  postBlog = (blog:Blog,file:File) =>{
    let formData = new FormData();
    blog.fileName = file.name;
    console.log(blog)
  
    formData.append('title',blog.title as string);
    formData.append('text',blog.text as string);
    formData.append('fileName',blog.fileName as string);
    formData.append('userBlogs',blog.userBlogs as string);
    formData.append('file',file,'form-data');
    
    return this.http.post(this.api+`/create`,formData);
  }

  putBlog = (blog:Blog) =>{
    return this.http.put(this.api+`/update/${blog._id}`,blog);
  }

  deleteBlog = (blog:Blog) =>{
    return this.http.delete(this.api+`/delete/${blog._id}`);
  }
}
