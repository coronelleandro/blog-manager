import { Injectable } from '@angular/core';
import { Blog } from '../models/blog';

@Injectable({
    providedIn: 'root'
})


export class CreateBlogValid{
    
    titleValid:String;
    textValid:String;
    fileValid:String;
    
    constructor(){
        this.titleValid = '';
        this.textValid = '';
    }

    validBlog(newBlog:Blog,file:File):Boolean{
        let valid = true; 
        if(newBlog.title == ''){
            this.titleValid = 'El titulo es necesario';
            valid = false;
        }
        else{
            this.titleValid = '';
        }

        if(newBlog.text == ''){
            this.textValid = 'El texto es requerido';
            valid = false;
        }
        else{
            this.titleValid = '';
        }

        if(file == null){
            this.fileValid = 'La imagen es requerida';
            valid = false;
        }
        else{
            this.fileValid = '';
        }

        return valid;
    }
}
