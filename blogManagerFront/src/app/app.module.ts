import { CommonModule } from '@angular/common';  
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BlogCrudComponent } from './components/blog-crud/blog-crud.component';
import { UserComponent } from './components/user/user.component';
import { BlogComponent } from './components/blog/blog.component';
import { RouterModule,Routes } from '@angular/router';



const routes:Routes = [
  {
    path:'angularBlog',
    children:[
      {
        path:'blogs',component:BlogCrudComponent,
      },
      {
        path:'user',component:UserComponent,
      },
      {
        path:'blog/:id',component:BlogComponent,
      }
    ]
  },
  {
    path:'',pathMatch:'full',redirectTo:'angulatBlog' 
  },
]

@NgModule({
  declarations: [
    AppComponent,
    BlogComponent,
    BlogCrudComponent,
    UserComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
