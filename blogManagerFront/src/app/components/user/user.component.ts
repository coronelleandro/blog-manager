import { Component,OnInit, Output,EventEmitter, ɵSWITCH_COMPILE_NGMODULE__POST_R3__ } from '@angular/core';
import { UserService } from '../../services/user.service';
import { NgForm } from '@angular/forms';
import { User } from '../../models/user';
import { Router } from '@angular/router';
import { PerfilService } from '../../services/perfil.service';
import { ValidateLogin } from '../../models/validateLogin';
import { CreateUser } from '../../models/createUser';
import { from } from 'rxjs';
import { sanitizeIdentifier } from '@angular/compiler';
 
@Component({
    selector:'app-user',
    templateUrl : './user.component.html',
    styleUrls:['./user.component.css'],
    providers:[UserService]
})

export class UserComponent implements OnInit{
    
    register:Boolean = null;

    confirmPassword:String = null;

    user:User;

    validateLogin:ValidateLogin;

    file:File;

    createUser:CreateUser;

    constructor(
        public router:Router,
        public userService:UserService,
        private perfilService:PerfilService 
        ) 
        {
            this.user = new User();
        }

    ngOnInit() {
        this.register = true;
        this.confirmPassword = '';
        this.validateLogin = new ValidateLogin();
        this.createUser = new CreateUser();
    }

    changeImage(event):void{
        let newFile = event.target.files[0];
        if(newFile.type.includes('image')){
            const reader = new FileReader();
            reader.readAsDataURL(newFile)
            reader.onload = function load(){
                console.log(newFile)
            }.bind(this);

            this.file = newFile;
        }
    }

    setRegister():void {
        this.register = true;
    }

    setSingIn():void {
        this.register = false;
    }

    setFocusInput(inputUser):void {
        inputUser.style.borderBottom = '2px solid red';
    }

    getUsers(user:User):void {
        this.userService.getOneUser(user._id)
            .subscribe(res=>{
                console.log(res)
            })
    }
    
    createNewUser(form:NgForm):void {
        //console.log(form.value);
        let validate = this.createUser.validateCreateUser(form.value,form.value.confirmPassword)
        if(validate){
            this.userService.getUserEmail(form.value.email)
            .subscribe(
                res=>{
                    if(res==null){
                        this.createPostUser(form)
                    }
                },
                error=>{console.log('ERROR: '+ error)}
            )
        }
    }

    createPostUser(formUser:NgForm):void{
        this.userService.postUser(formUser.value,this.file)
            .subscribe(res=>{
                console.log("exito")
            })
    }

    emitBack(event:any){
        alert(event)
    }

    signInUser(form:NgForm):void {
        
        let validate = this.validateLogin.validateLoginUser(form.value.email,form.value.password);
        if(validate==true){
            this.userService.getUserEmailAndPassword(form.value)
            .subscribe(res =>{
                if(res == null){
                    this.validateLogin.noFind();
                }
                else{
                    let userRes:User = res as User;
                    let id:string = userRes._id as string;
                    localStorage.setItem('idPerfil',id);
                    this.router.navigate(['angularBlog/blogs']);
                    
                }
            }) 
        } 
    }
}