import { Component,OnInit } from '@angular/core';
import { ActivatedRoute,Params } from '@angular/router';
import { Blog } from '../../models/blog';
import { BlogService } from '../../services/blog.service';
import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';
import { User } from '../../models/user';
import { PerfilService } from '../../services/perfil.service';

@Component({
    selector : 'app-blog',
    templateUrl : './blog.component.html',
    styleUrls : ['./blog.component.css'],
    providers: [BlogService]
})

export class BlogComponent implements OnInit{

    constructor(
        private activatedRoute:ActivatedRoute,
        private blogService:BlogService,
        private router:Router,
        private userService:UserService,
        public perfilService:PerfilService
    )
    {
        this.blog = new Blog();
    }

    ngOnInit(){
        this.idBlog = this.activatedRoute.snapshot.params.id;
        this.getUserPerfil();
        this.getOneBlogService(this.idBlog);
    }

    dateOfCreation:Date;

    idBlog:String; 

    blog:Blog;

    user:User;

    visible:Boolean = false;

    getUserPerfil():void{
        this.userService.getOneUser(localStorage.getItem('idPerfil'))
          .subscribe(res=>{
            this.perfilService.userPerfil = res as User;
          })
        
      }

    getOneBlogService(id:String):void{
        this.blogService.getOneBlog(id)
            .subscribe(res=>{
                this.setBlog(res);
                
            })
    }

    setBlog(value):void{
        this.blog._id = value._id;
        this.blog.title = value.title;
        this.blog.createDate = value.createDate as Date;
        this.blog.fileUrl = value.fileUrl;
        this.blog.fileName = value.fileName;
        this.blog.text = value.text;
        this.dateOfCreation = new Date(this.blog.createDate)
        this.getOneUserCreator(value.userBlogs);
    }

    getOneUserCreator(idUser){
        this.userService.getOneUser(idUser)
            .subscribe(res=>{
                this.user = res as User;
                console.log(this.user._id+"\n")
                console.log(this.perfilService.userPerfil._id)
                if(this.user._id==this.perfilService.userPerfil._id){
                    this.visible = true;
                }
            });
    }

    destroyBlog(blog:Blog):void{
        this.blogService.deleteBlog(blog)
            .subscribe(res=>{
                this.router.navigate(['angularBlog/blogs'])
            })
        
       console.log(blog)
    }
}