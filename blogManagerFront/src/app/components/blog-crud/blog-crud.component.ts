import { Component, OnInit } from '@angular/core';
import { Blog } from 'src/app/models/blog';
import { BlogService } from '../../services/blog.service';
import { NgForm } from '@angular/forms';
import { PerfilService } from '../../services/perfil.service';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';
import { CreateBlogValid } from '../../services/createBlogValid.service';
import { runInThisContext } from 'vm';

@Component({
  selector: 'app-blog-crud',
  templateUrl: './blog-crud.component.html',
  styleUrls: ['./blog-crud.component.css'],
  providers: [BlogService,UserService,CreateBlogValid]
})

export class BlogCrudComponent implements OnInit {

  file:File=null;

  constructor(
    public blogService:BlogService,
    public perfilService:PerfilService,
    public userService:UserService,
    public createBlogValid:CreateBlogValid
  ) { }

  ngOnInit():void {
    this.getAllBlogs();
    if('' != this.perfilService.userPerfil._id){
      this.getUserPerfil();
    }
  }

  changeImage(event):void{
    let newFile = event.target.files[0];
      if(newFile.type.includes('image')){
        const reader = new FileReader();
        reader.readAsDataURL(newFile)
        reader.onload = function load(){
          console.log(newFile)
          }.bind(this);

          this.file = newFile;
      }
  }

  getUserPerfil():void{
    this.userService.getOneUser(localStorage.getItem('idPerfil'))
      .subscribe(res=>{
        this.perfilService.userPerfil = res as User;
      })
    
  }

  getAllBlogs():void {
    this.blogService.getBlogs()
      .subscribe(res=>{
          this.blogService.blogs = res as Blog[];
          this.cleanForm();
        }
      );
  }

  cleanForm():void {
    this.blogService.theBlog = new Blog();
  }

  deleteBlog(blog:Blog):void {
    this.blogService.deleteBlog(blog)
      .subscribe(res=>{
        this.getAllBlogs();
        console.log("delete");
      })
  }

  selectBlog(blog:Blog):void{
    //console.log(blog)
    this.cleanForm();
    this.blogService.theBlog._id = blog._id;
    this.blogService.theBlog.title = blog.title;
    this.blogService.theBlog.text = blog.text; 
    this.blogService.theBlog.fileUrl = blog.fileUrl;
    this.blogService.theBlog.createDate = blog.createDate;
  }

  updateBlog(blog:Blog):void {
    this.blogService.putBlog(blog)
      .subscribe(res=>{
        this.getAllBlogs();
      })
  }

  addBlog(form:NgForm){  
    console.log(form.value);
    console.log(this.file);
    let newBlog:Blog = form.value as Blog;
    let valid = this.createBlogValid.validBlog(newBlog,this.file)
    if(valid){
      newBlog.userBlogs = this.perfilService.userPerfil._id;  
      console.log(newBlog);
      this.blogService.postBlog(newBlog,this.file)
      .subscribe(res=>{ 
          this.getAllBlogs();
        })
    }
    /*
    newBlog.userBlogs = this.perfilService.userPerfil._id;  
    console.log(newBlog);
    this.blogService.postBlog(newBlog,this.file)
     .subscribe(res=>{ 
        this.getAllBlogs();
      })
    */
  }
    
}
