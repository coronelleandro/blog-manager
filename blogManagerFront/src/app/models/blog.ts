import { User } from '../models/user';

export class Blog {

    constructor(id=null,title='',text='',fileName='',fileUrl='',userBlogs=''){
        this._id = id;
        this.title = title;
        this.text = text;
        this.fileName = fileName;
        this.fileUrl = fileUrl;
        this.userBlogs = userBlogs;
    }

    _id:String;
    title:String;
    text:String;
    fileName:String;
    fileUrl:String;
    createDate:Date;
    userBlogs:String;
}
