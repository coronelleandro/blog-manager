export class ValidateLogin{
    
    mailValidate:String;
    passValidate:String;

    constructor(){
        this.mailValidate = null;
        this.passValidate = null;
    }

    validateLoginUser(mail:String,password:String):Boolean{
        let validate = true;
        if(mail == ''){
            this.mailValidate = 'El mail es necesario';
            validate = false;
        }
        else{
            this.mailValidate = null;
        }
        if(password == ''){
            this.passValidate = 'El pasaword es necesario';
            validate = false;
        }
        else{
            this.passValidate = null;
        }
        return validate;
    }  

    noFind(){
        this.mailValidate = 'El mail o el password no coinciden';
        this.passValidate = 'El mail o el password no coinciden';
    }
} 