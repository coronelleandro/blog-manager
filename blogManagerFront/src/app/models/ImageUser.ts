export class ImageUser {
    
    constructor(fileName='',fileUrl='',uploadDate = null){
        this.fileName = fileName;
        this.fileUrl = fileUrl;
        this.uploadDate = uploadDate;
    }

    fileName:String;
    fileUrl:String;
    uploadDate:Date;
}
