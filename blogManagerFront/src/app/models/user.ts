import { ImageUser } from './ImageUser';

export class User{

    constructor(id='',name='',nameUser='',email='',password='',typeUser='user',fileName='',fileUrl=''){
        this._id = id;
        this.name = name;
        this.nameUser = nameUser;
        this.email = email;
        this.password = password;
        this.typeUser = typeUser;
        this.fileName = fileName;
        this.fileUrl = fileUrl;
    }

    _id:String;
    name:String;
    nameUser:String;
    email:String; 
    password:String; 
    typeUser:String;
    fileName:String;
    fileUrl:String;

}