import { IfStmt } from '@angular/compiler';
import { User } from './user'

export class CreateUser{
    
    nameValidate:String;
    nameUserValidate:String;
    emailValidate:String; 
    passwordValidate:String;
    imageUserValidate:String; 
    confirmPassValidate:String;

    constructor(){
        this.nameValidate = null;
        this.nameUserValidate = null;
        this.emailValidate = null;
        this.passwordValidate = null;
        this.imageUserValidate = null;
        this.confirmPassValidate = null;
        
    }

    validateCreateUser(user:User,confirmPassword:String):Boolean{
        let validate = true;
        if(user.name ==''){
            this.nameValidate = 'El nombre es necesario';
            validate = false;
        }
        else{
            this.nameValidate = null;
        }
        if(user.nameUser ==''){
            this.nameUserValidate = 'El nombre de usuario es necesario';
            validate = false;
        }
        else{
            this.nameUserValidate = null;
        }
        if(user.email ==''){
            this.emailValidate = 'EL mail es necesario';
            validate = false;
        }
        else{
            this.emailValidate = null;
        }
        if(user.password ==''){
            this.passwordValidate = 'El password es requerido';
            validate = false;
        }
        else{
            this.passwordValidate = null;
        }
        if(confirmPassword != user.password){
            this.confirmPassValidate = 'Los 2 password no son iguales';
            validate = false;
        }
        else{
            this.confirmPassValidate = null 
        }
        
        return validate;
    }
}