Web para crear temas de blogs (MEAN)

Se trata de una practica en la que hay una pagina de blog en la que todos los usurios pueden leer el contenido del mismo, pero tienen que registrarse en caso de que quieran entrar y poder crear articulos nuevos para el blog.

Los datos se guardan y se obtienen de una api creada por mi con nodejs, mongodb y con paquetes npm como express, mongoose, multer y nodemon.    

Esta practica esta hecha con angular para el front-end, nodejs para el back-end y mongodb para manejar los datos 
 
