const express = require('express');
const router = express.Router();
const multer = require('multer');
const storage = require('../config/multer');
const User = require('../models/user');

const uploader = multer({
    storage
}).single('file')

const userCtrl = require('../constrollers/user.controller')

router.get('/',userCtrl.getAllUsers)

router.get('/:id',userCtrl.getOneUser)

router.get('/email/:email',userCtrl.getUserEmail)

router.post('/create',uploader,userCtrl.postCreateUser)

router.delete('/delete/:id',userCtrl.deleteUser)

router.get('/:email/:password',userCtrl.getUserPassAndEmail)

module.exports = router;