const express = require('express');
const router = express.Router();
const multer = require('multer');
const storage = require('../config/multer');

const uploader = multer({
    storage
}).single('file')

const blogCtrl = require('../constrollers/blog.controller')

router.get('/',blogCtrl.getAllBlogs);

router.get('/:id',blogCtrl.getOneBlog);

router.post('/create',uploader,blogCtrl.createBlog);

router.put('/update/:id',blogCtrl.updateBlog);

router.delete('/delete/:id',blogCtrl.deleteBlog);

module.exports = router;  