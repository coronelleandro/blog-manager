const userCtrl = {}
const User = require('../models/user');

userCtrl.getAllUsers = async (req,res) =>{
    const users = await User.find();
    res.json(users);
}

userCtrl.postCreateUser = async (req,res) =>{
    const {body,file} = req;
    const newUser = new User({
        name: body.name,
        nameUser: body.nameUser,
        email: body.email,
        password: body.password,
        fileName: body.fileName,
        typeUser: body.typeUser,
        fileName: body.fileName,
        fileUrl: `http://localhost:3000/${file.filename}`,    
    }); 
    await newUser.save(newUser);
    res.json({
        'estado': 'saveado'
    });
}

userCtrl.getOneUser = async (req,res) =>{
    const user = await User.findById(req.params.id);
    res.json(user);
}

userCtrl.getUserEmail = async (req,res)=>{
    const user = await User.findOne({ email: req.params.email })
    res.json(user);
}

userCtrl.deleteUser = async (req,res) =>{
    await User.findByIdAndDelete(req.params.id); 
    res.json({
        'status':'deleteado'
    })
}

userCtrl.getUserPassAndEmail = async (req,res) =>{
    const user = await User.findOne({
                            $and:[
                                { email: req.params.email },
                                { password: req.params.password }
                            ]
                    })
    res.json(user);
}


module.exports = userCtrl;