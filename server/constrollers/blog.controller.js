const blogCtrl = {};
const Blog = require('../models/blog');
var mongoose = require('mongoose');

blogCtrl.getAllBlogs = async (req,res) =>{
    const blogs = await Blog.find()
    res.json(blogs)
}

blogCtrl.createBlog = async (req,res) =>{
    const {body,file} = req;
    const blog = new Blog({
        title: body.title,
        text: body.text,
        fileName: body.fileName,
        userBlogs: body.userBlogs,
        fileUrl: `http://localhost:3000/${file.filename}`,
    })
    await blog.save(blog)
    res.json({
        'status':'recivido'
    })
}

blogCtrl.updateBlog = async (req,res) =>{
    const {id} = req.params
    const blog = {
        title: req.body.title, 
        text: req.body.text,
        image: req.body.image,
        createDate: req.body.createDate, 
        userBlogs:req.body.idUser
    }
    await Blog.findByIdAndUpdate(id , {$set: blog} , {new: true})
    res.json({
        'status':'actualizado'
    })
}

blogCtrl.getOneBlog = async (req,res) =>{
    const blog = await Blog.findById(req.params.id)
    res.json(blog)
}

blogCtrl.deleteBlog = async (req,res) =>{
    await Blog.findByIdAndDelete(req.params.id)
    res.json({
        'status':'deleteado'
    })
}

module.exports = blogCtrl;