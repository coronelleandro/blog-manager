const mongoose = require('mongoose');
const { Schema } = mongoose;

const userSchema = new Schema({
    name: { type: String , require: true },
    nameUser: { type: String , require: true },
    email: { type: String , require: true },
    password: { type: String , require: true },
    fileName: { type:String , require: true},
    fileUrl: { type:String , require: true },
    typeUser: { type: String , require: true },
    
});

module.exports = mongoose.model('user',userSchema);