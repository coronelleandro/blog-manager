const mongoose = require('mongoose');
const { Schema } = mongoose;

const blogSchema = new Schema({
    title: { type: String, require: true },
    text: { type: String, require: true },
    fileName:{ type:String ,requiere:true },
    fileUrl: { type:String, require:true },
    createDate: { type: Date, default: Date.now },
    userBlogs: { type:String ,require: true },
});

module.exports = mongoose.model('blog',blogSchema);