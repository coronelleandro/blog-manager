const express = require('express');
const app = express();
const morgan = require('morgan'); 
const {mongoose} = require('./config/database');
const cors = require('cors');
const path = require('path');

//settings
app.set('port',process.env.PORT || 3000);

//middlewares
app.use(morgan('dev'));
app.use(express.json());
app.use(cors({origin:'http://localhost:4200'}));

//static
app.use(express.static(path.join(__dirname,'upload')));

//routes
app.use('/api/blogs/',require('./routes/blog.routes'));
app.use('/api/users/',require('./routes/user.routes'));

app.listen(app.get('port'),()=>{
    console.log('server iniciado puerto 3000 '+__dirname);
})